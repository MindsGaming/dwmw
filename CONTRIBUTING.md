## Contribution

Yes! Please submit a request for improvement! 

Here are some things that help if you're not sure how to help 🚀🐰🎉

📝Editing:  
We are not perfect and make errors, reviewing documents for spelling, grammar and constant use of terminology can always help. 

🕸️ Resources: 
Adding resources and information to the wiki page such as tokens, channels, tube locations, and more while removing any outdated information helps keep the project organized.

🧪Test:
Entities (prompts) are created by our main language module, testing, using and building stories with these prompts while reporting and fixing any issues if possible.

📽️Upload:
If you help with the video projects be voice, avatar moving, script creating or anything else please add the file to the appropriate folder, if no appropriate folder exists create a new folder. 


🚀Other Things: 
Join us in building this project on Minds.com so many other ways to help improve this project by simply participating in the project! 
 
